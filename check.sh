#!/bin/bash 

# This script was written to check for the following 
# information
# 1). mongodb.service status 
# 2). check if the root_pass env variable is still present
# 3). check the log file and output the tail -n 2 value 
# 4). 

# Getting the root user password 
rootUserPassword=$root_pass; 
echo "$root_pass" > passd.bin; 

# Getting the path to the mongodb log file 
logFilePath="/var/log/mongodb/mongod.log"; 

# Checking if the root_pass is present 
if [[ $rootUserPassword ]]
then
    # If the root_pass environment variable is present, execute the 
    # code block below 
    echo "Password env enabled."; 

    # Checking the status of the mongodb server using escalated system privileges
    mongodbStatus=$(printf "$rootUserPassword" | sudo -S systemctl status mongod.service | grep "Active" | cut -c 13-100);

    # Displaying the status for the mongodb server 
    echo "[MESSAGE]:$mongodbStatus"; 

    # Working with the mongodb log file 
    logFileContents=$(printf "$rootUserPassword" | sudo cat "$logFilePath" | tail -n 2); 

    # Viewing the log file contents 
    echo "Contents:  $logFileContents"; 

# Else if the password env is not enabled 
else
    # Execute this block of code if the password env is not enabled 
    echo "Password env is not enabled."; 

    # Closing up the if/else statement 
    fi; 
