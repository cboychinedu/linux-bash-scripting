## Saving anaconda environments 
$ conda env export -n <environment-name> -f my-environment.yml 

## OR 
$ conda env export > my-environment.yaml 

#### Creating an anaconda environment from a yaml file 
$ conda env create --file environment.yaml 

## Finding conda environments on your system 
$ conda env list 















