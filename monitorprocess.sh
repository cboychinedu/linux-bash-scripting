#!/bin/bash 

# This script is for just process manipulations 
# Get the process of vlc media player 
# Setting the name of vlc log file 
vlcLogFile="vlc.log"; 

# Creating the log file/ but firstly check if the file is present already 
if [[ -e $vlcLogFile ]]
then
    # If the "vlc.log" file is present, check for permissions and also if the file is empty 
    if [[ -w $vlcLogFile && -s $vlcLogFile ]]
    then
        # If the file has a write permission, and also not empty 
        echo "[MESSAGE]: The file is present and contains data.";

    else
        # If the file does not have the write permission, change the permission of the file 
        # to read, write and execute 
        # Changing the group ownership 
        chown $USER:$USER $vlcLogFile; 

        # Making the file readable, writeable and executable. 
        chmod +x $vlcLogFile;  

        # Closing 
        fi; 

# If the "vlc.log" file is missing, create a new log file and change it's permissions
# to read, write, and execute 
else 
    # Displaying a message for file not found. 
    echo "[MESSAGE]: The file $vlcLogFile is not present on disk!"; 
    sleep 2; 

    # Creating the "vlc.log" file on disk 
    echo "[MESSAGE]: Creating a new log file with name: $vlcLogFile"; 
    touch "$vlcLogFile";  

    # Changing its permissions 
    chown $USER:$USER $vlcLogFile; 

    # Displaying the status 
    echo "[MESSAGE]: File $vlcLogFile created!"; 

    # Closing the statement 
    fi; 


# Saving the PID into the log file 
pgrep vlc >> $vlcLogFile; 

# Reading the vlc log file back into memory and saving it's contents inside 
# A variable 
while IFS= read -r file
do
    # Execute this command when reading the log file 
    LogFileContents=$file; 
    echo "[MESSAGE]: The PID value is: $LogFileContents"; 

# Reading the log file 
done < $vlcLogFile; 

# Closing vlc media player 
echo "[MESSAGE]: Closing the media player."; 
kill -INT $LogFileContents; 

# Deleting the log file 
rm -rf $vlcLogFile; 

