## Listing a running service or services
$ systemctl list-units --type=service

OR
$ systemctl --type=service

# Listing all loaded but only active services
$ systemctl list-units --type=service --state=active
$ systemctl --type=service --state=active

# List all loaded but running services
$ systemctl list-units --type=service --state=running
$ systemctl --type=service --state=running 


## Creating a service 
$ cd /etc/systemd/system 
$ ls -l 

## To get what the system loads at boot time, the services 
## are listed inside the "multi-user.target.wants/" directory 
$ ls multi-user.target.wants/ 

## To create a service that runs at boot time, you create a file with 
## The extension of ".service" 
$ vim connection.service 

#####
"""
[Unit]
Description=making network connection up 
After=network.target 

[Service]
ExecStart=/root/scripts/conup.sh 

[Install]
WantedBy=multi-user.target 
"""
#####

## Enable the service 
$ systemctl enable connection.service 

## To check if the service has been enabled, we look inside the 
## "multi-user.target.wants/" directory 
$ ls multi-user.target.wants/ 

## To start the service immidiately, we issue the following commands 
$ systemctl start connection.service 


