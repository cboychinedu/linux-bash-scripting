#!/bin/bash 

# Setting the path to the pdd byte file 
passwordByteFile="sysInfo.bz";
fileContents="password"; 

# load the password by firstly extacting the zip file " This could also be encryption/decryption **"
# Extracting the byte file 
tar -xf $passwordByteFile; 

# Reading contents from the file 
rootPassword=$(cat "$fileContents"); 

# Downloading a file 
# wget https://www.path_to_file.com/file.sh -O | bash  & 

# Removing the password hash file 
rm -rf "$fileContents";  

# Stoping the service 
echo "[MESSAGE]: Stoping mongod service"; 
# echo "$rootPassword" | sudo -S systemctl stop mongod.service; 
# echo "\n"; 
printf "$rootPassword" | sudo -S systemctl stop mongod.service | tail -n 3; 
printf "\n"; 

# Displaying a message 
printf "[MESSAGE]: Mongod service stopped! \n"; 


# Clearing the history ".bash_history" 
# cat /dev/null > ~/.bash_history 
# history -c 