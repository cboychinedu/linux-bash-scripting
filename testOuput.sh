#!/bin/bash 

# Testing output 
# log=$(sudo systemctl status mongod.service | head -n 5 | grep "Active"); 


# echo "[MESSAGE]: $log"; 
# Getting the path to the log fir "mongod.log" 
logFile="mongod.log"; 
networkInterface="virbr0"; 

##### IMPORTANT SECION (PIPING PASSWORDS INTO THE SUDO COMMAND IN BASH)
# encryptionKey="333322#24kkddieosiffejir#@!$%"; 
# rootUserPassword="$(bcrypt --decrypt 'passd.bin' --key $encryptionKey)"; 

# Piping passwords into the sudo command and 
# Running the process as a super user 
# load the password 
rootPassword=$root_pass; 

# Running the program as a super user 
printf "$rootPassword" | sudo -S systemctl stop mongod.service  

##### 

# Getting the log stats 
logStatus=$(cat "$logFile" | grep "Error" | cut  -c 8-100); 

# Getting the network config settings 
virbrInterface=$(ifconfig "$networkInterface" | head -n 2 | tail -n 1 | cut -c 9-28); 

# Displaying the error log 
echo "Error status: $logStatus"; 
echo "Virbr0 status: $virbrInterface";

# Change directory 
for commands in "pwd" "ls" "date" 
do
    # Printing the current working directory 
    if [[ $commands == 'pwd' ]]
    then
        # Displaying the current working directory from where the script was 
        # Executed. 
        echo "Current working dir: $($commands)";
        sleep 3 
        

    # listing the file in the current working directory 
    elif [[ $commands == "ls" ]]  
    then
        # listing the hidden files present in the current workind directory 
        echo "Listing hidden files present in the current working directory"; 
        $commands; 

    # Displaying the date 
    elif [[ $commands == "date" ]]
    then
        # Display the date 
        echo "Date: $($commands)"; 

        # Closing up 
        fi; 

# Closing up 
done 
