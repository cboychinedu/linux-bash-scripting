#!/bin/bash 

# Bash exercise 
# - Write a bash script to disable the xinput id:10 
# - grep the network configuration

# listing the connected devices 
xinput list | head -n 10; 

# Delaying for some few seconds 
sleep 2; 

# Disabling the screen 
xinput disable 10; 

# Display message 
echo "Disabled screen touch"; 

# Checking 
# Creating a file for the mongod logs 
mongodbLog="mongod.log"; 
generalLog="generalLog.log"; 


# Checking if the file is present on disk 
if [[ -e $mongodbLog ]]
then
    # If the file is present, just append new log values to it 
    # Getting the log status and appending it's values into the log file if present. 
    sudo systemctl status mongod.service | head -n 5 | grep "Active" >> $mongodbLog;

    # Changing permission 
    chmod 777 $mongodbLog; 

    # Displaying the status message 
    echo "[INFO]: Saved log stats in: $mongodbLog"; 

elif [[ -e $generalLog ]]
then
    # If the mongoLog file is not present, look for the general log file, 
    # and appending the log values into it 
    sudo systemctl status mongod.service | head -n 5 | grep "Active" >> $generalLog;

    # Changing permission 
    chmod +x $generalLog; 

    # Displaying the status message 
    echo "[INFO]: Saved log stats in: $generalLog";  

else
    # Execute this block of code if both files are not present on disk 
    # Create both log files "mongod.log" and "generalLog.log", 
    # Save the log value into both file and close the stream 
    touch "$mongodbLog" "$generalLog"; 
    sudo systemctl status mongod.service | head -n 5 | grep "Active" >> $mongodbLog; 
    sudo systemctl status mongod.service | head -n 5 | grep "Active" >> $generalLog; 

    # Changing permission 
    chmod +x *.log 

    # Displaying the status message 
    echo "[INFO]: Saved log stats in: $mongodbLog"; 
    echo "[INFO]: Saved log stats in: $generalLog"; 

    # Closing up 
    fi; 