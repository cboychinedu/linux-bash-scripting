#!/bin/bash 

########
# -f flag: this will check for only files and files only. 
# -d flag: this will check for directory and directory only. 
# -s flag: this will check if the file is empty or not 
# -r flag: this will check if the file has a read permission 
# -w flag: this will check if the file has a write permission 

########

# Reading the users input on a single line
read -p 'Enter the name of the file : ' file_name; 

# Checking if the file exists 
if [[ -e $file_name ]]  
then
    # Displaying the found file with it's file name 
    echo "$file_name found"; 

    # Checking if the found file is empty or not 
    if [ -s $file_name ]
    then
        # Displaying the result if the file is not empty 
        echo "The file $file_name is not empty"; 
    else
        # Execute this block of code if the file is empty 
        echo "The file $file_name is empty"; 
        fi; 

else
    echo "$file_name not found";
    fi; 

# looking for the specified directory 
if [ -d $file_name ]
then 
    echo "The directory with the name $file_name exists "; 

else
    echo "The directory with the file name $file_name does not exit"; 

# Closing 
fi 