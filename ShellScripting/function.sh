#!/bin/bash 

# Creating a name variable 
firstName="Mbonu";
lastName="Chinedum";  

# Creating a function to take in arguments
function main() 
{
    printf "Hello $1 \n"; 
}

# Creating another function to get and output the full name 
function outputFullName()
{
    fullNameDetails="$1 $2"; 
    echo "Hello, $fullNameDetails"; 
}

# Calling the function "main()" and parsing in argument of "Mbonu Chinedum" 
# Into the function.  
# main $firstName; 
outputFullName $firstName $lastName;

# Displaying the password 
echo "Password: $app_password"; 


