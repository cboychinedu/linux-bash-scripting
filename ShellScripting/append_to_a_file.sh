#!/bin/bash 

########
# > flag: This is a single redirection, here a new file is created 
# >> flag: This is a double redirection, here data from the stream is appended into the specified file. 

# Setting the path to the file 
file_name="file1"; 

# Creating the data for writing into the file 
data="
Hello, my name is Mbonu Chinedum.
I love bash shell scripting. 
And i hope you love it too. 
"; 

# Checking if the file exists on disk 
if [[ -e $file_name ]]
then    
    # Checking if the file has a write permission 
    if [[ -w $file_name ]]
    then
        # If the file has a write permission, then write some documents 
        # Into the file 
        echo "Writing documents into the file"; 
        echo "$data" >> $file_name; 
    else
        # If the file does not have a write permission 
        echo "The file does not have a write permission"; 
        fi;  

# If the file does not exist 
else 
    echo "The file does not exist"; 
    echo "Creating the file"; 

    # Creating the file with the specified name 
    touch $file_name; 

    # Changing the write permissions 
    chmod +x $file_name; 

    # Then writing the documents to the file 
    echo "Writing data to the newly created file"; 
    echo "$data" >> $file_name; 
    fi; 

# Checking the permission of the file now 
ls -ltr | grep "$file_name"; 
