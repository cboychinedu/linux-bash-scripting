#!/bin/bash 

# Working with the logical 'AND' operator 
age=25; 
name="Mbonu Chinedum"; 

# Using the logical 'AND' operator 
if [[ $age > 20 && $name == "Mbonu Chinedum" ]]
then 
    echo "The condition is true";
else
    echo "The condition is not true"; 
    fi; 