#!/bin/bash 

# Reading file using while loops 
# while read p  
# do 
#     echo $p 

# done  < read_files.sh 

## Process 2 
# cat read_files.sh | while read file 
# do 
#     echo $file; 

# done 

## Process 3 
while IFS= read -r line 
do 
    echo $line 

done < mongodb.log 