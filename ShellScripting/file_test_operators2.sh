#!/bin/bash 

# Using for-loop to work on the file located in the 
# Current working directory 
for fileNames in *
do 
    # Checking if the item is a directory 
    if [[ -d $fileNames ]]
    then
        # If the item is a directory, change the ownership to the user 
        chwon $USER:$USER $fileNames; 
        echo "Directory: $fileNames"; 

    # Checking if the file found is not empty 
    elif [[ -s $fileNames ]]
    then   
        # If the file is empty, then display this message 
        echo "File: $fileNames : is not empty"; 

    # Getting empty file 
    else
        # For empty file 
        echo "File: $fileNames : is empty"; 

        # Closing up 
        fi; 

# Closing up the for loop  
done; 