#!/bin/bash

# This is a comment
echo "Hello World"

# Checking the bash type
echo "Our shell name is $BASH"
echo "The bash version is $BASH_VERSION"
echo "Our home directory is $HOME"
echo "Our current working directory is $PWD"; 

# Creating a variable to hold the name of someone
name=Mark

# Displaying the name
echo "The name is $name "

# Reading the users input on a single line
read -p 'username : ' user_name

# Reading the password
read -sp 'password: ' user_pass


# Displaying the inputs
echo "The username is: $user_name"
echo "The password is: $user_pass"
