#!/bin/bash 

# Using the break and continue statement 
for (( i=0; i<=10; i++ ))
do 
    echo "Counting: $i"; 

    # if-block condition 
    if [[ $i > 5 ]]
    then   
        # if the value for i is greate than 5, break 
        echo "The value for i is: $i"; 
        echo "Broken out of the loop"; 
        break; 
    
    # Else block if the "if-block" condition was not satified 
    else
        # Continue the looping process 
        continue; 

        # closing up 
        fi; 

done; 