#!/bin/bash 

# Using if-statements 
# if [ condition ]
# then
#     statement 
#     fi 

# else
#     statement
# Creating a variable called count 
count=10; 
word="a"; 

# Using the if/else statement 
if !(( $count == 11 ))
then 
    # Execute the block of code below if the condition is true 
    echo "The condition is true"; 

elif (( $count == 10 ))
then
    # Using the else-if statement 
    echo "The count is '10' "; 


else
    # Execute this block of code if the condition is false 
    echo "The condition is false"; 
    fi 

if (( $word == "a" ))
then
    # Execute this block of code is the condition is true 
    echo "The variable word has a value of 'a' "; 

else 
    # Execute this block of code if the condition is false 
    echo "The variable word does not have a value of 'a' "; 

# Closing the application 
fi 