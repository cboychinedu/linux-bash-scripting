#!/bin/bash 

# Working with the select loop in bash 
# Creating a list to hold certain values 
osNames=('Ubuntu' 'Windows' 'kali' 'Parrot Os'
        'Fedora' 'Red Hat' 'Mint' 'Arch Linux' 
        'Pop Os' 'Chrome Os' 'Mac OS'); 

# Selecting some specific os
select name in ${osNames[@]}
do 
    echo "$name selected"; 
    
    # Removing the selected os name 
    for (( i=0; i<${#osNames[@]}; i++ ))
    do
        # Execute this block of code to get the os selected names, and 
        # check if the selected name equals the actual name in the list
        if [[ $name == ${osNames[i]} ]]
        then
            # Execute this block of code if the selected name is present in the osNames list 
            unset osNames[i]; 
            echo "$name removed";

            # Closing up 
            fi; 
            
    # Closing up 
    done; 

    # Displaying the new list 
    echo "List: ${osNames[@]}";  

# Closing up 
done; 