#!/bin/bash 

# function-2 
# Creating a function to check fo the presence of a file on disk 
function is_file_exist() {
    local file="$1"; 

    # Checking if the file does not exist 
    # If the file does not exist, execute the block of code inside the if-block 
    if ! [[ -f $file ]]
    then
        # Execute this code if the file does not exist 
        echo "File: $file does not exist"; 

    else
        # Execute this block of code is the file exists 
        echo "File: $file exists"; 

        # Closing up the program 
        fi; 
}

# Running the function to check if the file with name "win32log.bat" exists. 
is_file_exist "win32log.bat"; 