#!/bin/bash 
# For loops in bash 

#### Case 1 
# Working with output 
for OUTPUT in $(echo "hello world!")
do  
    echo "$OUTPUT"

done 

#### Case 2 
# looping for numbers from 1 to 10 
for numbers in {1..10}
do
    # Displaying the numbers 
    echo "The numbers are: $numbers"; 

# Closing up the for-loop 
done 


#### Case 3 
# Creating a list to hold certain values 
osNames=('Ubuntu' 'Windows' 'kali' 'Parrot Os'
        'Fedora' 'Red Hat' 'Mint' 'Arch Linux' 
        'Pop Os' 'Chrome Os' 'Mac OS'); 

# Extracting the values from the osNames 
for indexValue in ${!osNames[@]}
do
    # Displaying the os names 
    echo ${osNames[$indexValue]}; 

# Closing up the for-loop 
done 


#### Case 4 
for (( i=0; i<${#osNames[@]}; i++ ))
do 
    # Execute this block of code if the condition is satified 
    echo "${osNames[i]}"; 

# Closing up the for-loop 
done 
