#!/bin/bash 

# Getting the current user 
localUser=$(echo "$USER");

# Arithmetic operations 
num1=20
num2=5; 

# Creating a variable to hold a string 
user_name="Mbonu Chinedum"; 

# Finding the length of characters in the 'user_name' variable 
echo "The user name is: $user_name "; 
echo "The home user is: $localUser "; 
echo "The number of characters: ${#user_name[0]}"; 

# 
echo "Addition: $(( num1 + num2 ))"; 
echo "Subtraction: $(( num1 - num2 ))"; 
echo "Multiplication: $(( num1 * num2 ))"; 
echo "Division: $(( num1 / num2 ))"; 

# 
# app_passD="1234567890"; 
# echo "export app_password=$app_passD" | bash 
# echo "export app_password=123456" | bash 

# Creating a list to hold certain values 
osNames=('Ubuntu' 'Windows' 'kali' 'Parrot Os'
        'Fedora' 'Red Hat' 'Mint' 'Arch Linux' 
        'Pop Os' 'Chrome Os' 'Mac OS'); 

# Displaying 
echo "The contents of the array: ${osNames[@]}"; 
echo "The first value: ${osNames[0]}";
echo "The index numbers: ${!osNames[@]}"; 
echo "The length of items in the array: ${#osNames[@]}"; 

# Removing elements from the array 
unset osNames[2];      # >> We removed Kali here 

# Displaying the changes 
echo "The contents of the array: ${osNames[@]}"; 

