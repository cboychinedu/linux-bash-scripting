#!/bin/bash 

# Working with while loops 
n=1; 

# 
# while [[ $n -le 10 ]]
# do
#     echo "$n"; 
#     # n=$(( n + 1 )); 
#     (( n++ )); 

# done 

# Opening file using while loop 
while [[ $n -le 3 ]]
do
    echo "Opening terminal";
    # Opening the xterm terminal to open vlc media player minimized.  
    xterm vlc & 
    # gnome-terminal & 

    # Incrementing the values for n 
    (( n++ )); 

# Closing up the while loop 
done; 
