#!/bin/bash

## Symbolic Links in linux
### Creating a soft Symbolic link in linux for a file
"""
Note that the command below would create the link file in the current
working directory. You can also reference a file path from another directory with
the full path, and the symbolic link would be created in the current working directory.
"""
$ ln -s target_file link_name
$ ln -s <file_name> <Symbolic_link_name>
$ ln --symbolic <file_name> <symbolic_link_name>

## Creating a soft symbolic link in linux for a folder
$ ln -s </folder/folder_name>  <symbolic_link_folder>
$ ln -s /home/kelvin kelvin_folder

## Removing symlinks
$ unlink <path-to-symlink>
$ rm <path-to-symlink> 
