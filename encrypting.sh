#!/bin/bash 

# Getting the root user password and 
# Setting the path to "passd.bin" file 
rootUserPassword=$root_pass; 
filePath="passd"; 

# Setting the path to the encryption file 
encryptedFile="passd.cpy"; 

# Saving the password to the filePath on disk "passd"
echo "$root_pass" > "$filePath";  

# Encrypting the password file "passd" to "passd.cpy"  
crypt "$token_pass" < "$filePath" > "$encryptedFile" &>/dev/null; 

# Changing the file permissions 
chmod +x "$encryptedFile"; 

# Decrypting the password file "passd.cpy" OR $encryptedFIle 
# crypt "$token_pass" < "$encryptedFile" > "$filePath"; 

# Removing the "passd" file 
rm -rf "$filePath"; 

# Displaying the status message 
printf "[MESSAGE]: File ' $filePath' encrypted. \n"; 


